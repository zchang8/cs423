# CS423-MP3

##  How to run the program

Download
------
    git clone https://github.com/cs423-uiuc-sp20/proj_sp20_f1mm.git

### Build the kernel module
before install the kernel, we need to build the kernel
```
cd theFolderYouChoose
make

```
after this, there should be a file named mp3.ko

### Install the kernel module 
In order to use the moudule, we need to install the kernel into our system, the command show below

```
sudo insmod mp3.ko
```

### Check the install status
You can check the status by this command

```
lsmod
```
If the install succeed, there will be an entry named mp3

### Build Node
In order to use the character devices, we need to make a node, first we need to get the device number made by the mp3.ko.
```
cat /proc/devices
```
find the device number of mp3.

Then make a node
```
sudo mknod node c mp3_device_number 0
```

### How to use the module
You can test the kernel by running the work task which is a programm registered the kernel module

```
./work memory_use_count memory_access_mode iteration_count
```
Then you can get the result by 
```
sudo ./monitor > result_file
```
you can get the profile result in the file

### How to uninstall the module
You can remove the module by
```
sudo rmmod mp3
```
