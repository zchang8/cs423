#define pr_fmt(fmt) "cs423_mp4: " fmt

#include <linux/lsm_hooks.h>
#include <linux/security.h>
#include <linux/kernel.h>
#include <linux/err.h>
#include <linux/cred.h>
#include <linux/dcache.h>
#include <linux/binfmts.h>
#include "mp4_given.h"

typedef struct mp4_security mp4_security_t;
#define XATTR_LEN 64
#define PATH_BUF 512
/**
 * get_inode_sid - Get the inode mp4 security label id
 *
 * @inode: the input inode
 *
 * @return the inode's security id if found.
 *
 */
static int get_inode_sid(struct inode *inode)
{
	/*
	 * Add your code here
	 * ...
	 */
	/*char *buf;
	struct dentry *dentry;

	int rc;
	int xattr_cred;

	if(!inode){
		if(printk_ratelimit()){
			pr_alert("input inode structure is nullptr at get_inode_sid\n");
		}
		return -1;
	}

	if(!(inode->i_op->getxattr)){
		// not impletment handler
		//if(printk_ratelimit()){
			//pr_alert("input inode structure does not impletment getxattr\n");
		//}
		return MP4_NO_ACCESS;
	}

	dentry = d_find_alias(inode);
	if(!dentry){
		//if(printk_ratelimit()){
			//pr_alert("cannot find dentry for inode at get_inode_sid\n");
		//}
		return -1;
	}

	buf = kmalloc(XATTR_LEN, GFP_KERNEL);
	if(!buf){
		if(printk_ratelimit()){
			pr_alert("fail to allocate memory for xattr buf entry at get_inode_sid");
		}
		dput(dentry);
		return -ENOMEM;
	}
	buf[XATTR_LEN - 1] = '\0';

	rc = inode->i_op->getxattr(dentry, XATTR_NAME_MP4, buf, XATTR_LEN);
	dput(dentry);
	xattr_cred = MP4_NO_ACCESS;
	if(rc && rc != -ENODATA){
		buf[rc] = '\0';
		xattr_cred = __cred_ctx_to_sid(buf);
	}
	kfree(buf);
	return xattr_cred;*/
	char * cred_ctx;
	struct dentry *de = d_find_alias(inode);
	int ret, sid, len;

	if (!de) {
		pr_err("%s, NULL dentry\n", __func__);
		return -EFAULT;
	}

	len = XATTR_LEN;
	cred_ctx = kmalloc(len, GFP_NOFS);
	if (!cred_ctx) {
		dput(de);
		pr_err("%s, could not allocate mem\n", __func__);
		return -ENOMEM;
	}
	// Clean the momory 
	memset(cred_ctx, 0, len);

	if (!inode->i_op->getxattr) {
		dput(de);
		kfree(cred_ctx);
		return 0;
	}

	ret = inode->i_op->getxattr(de, XATTR_NAME_MP4, cred_ctx, len);

	if (ret == -ERANGE) {
	    ret = inode->i_op->getxattr(de, XATTR_NAME_MP4, NULL, 0);
	    if (ret < 0) {
		dput(de);
		pr_err("%s, query the correct size of xattr failed", __func__);
		return ret;
	    }

	    len = ret;
	    cred_ctx = kmalloc(len + 1, GFP_NOFS);
	    if (!cred_ctx) {
		dput(de);
		return -ENOMEM;
	    }
	    cred_ctx[len] = 0;
	    ret = inode->i_op->getxattr(de, XATTR_NAME_MP4, cred_ctx, len);
	}

	dput(de);

	if (ret < 0) {
	    if (ret != -ENODATA) {
		pr_err("%s, get the xattr failed\n", __func__);
		kfree(cred_ctx);
		return ret;
	    }

	} else {
	    sid = __cred_ctx_to_sid(cred_ctx);
	    ret = sid;
	}

	kfree(cred_ctx);
	return ret;
}

/**
 * mp4_bprm_set_creds - Set the credentials for a new task
 *
 * @bprm: The linux binary preparation structure
 *
 * returns 0 on success.
 */

static int mp4_cred_alloc_blank(struct cred *cred, gfp_t gfp);

static int mp4_bprm_set_creds(struct linux_binprm *bprm)
{
	/*
	 * Add your code here
	 * ...
	 */
	mp4_security_t* sec;
	int sec_flags;

	if(!bprm){
		if(printk_ratelimit()){
			pr_alert("input bprm is null at mp4_bprm_set_creds\n");
		}
		return -EINVAL;
	}
	if(!bprm->cred){
		if(printk_ratelimit()){
			pr_alert("input bprm's cred field is null at mp4_bprm_set_creds\n");
		}
		return -EINVAL;
	}
	sec = bprm->cred->security;

	if(!sec){
		if(printk_ratelimit()){
			pr_debug("input bprm's security field is null, create blank one at mp4_bprm_set_creds\n");
		}
		mp4_cred_alloc_blank(bprm->cred, GFP_KERNEL);
		sec = bprm->cred->security;
	}

	sec_flags = get_inode_sid(bprm->file->f_inode);
	if(sec_flags == MP4_TARGET_SID){
		sec->mp4_flags = MP4_TARGET_SID;
	}
	return 0;
}

/**
 * mp4_cred_alloc_blank - Allocate a blank mp4 security label
 *
 * @cred: the new credentials
 * @gfp: the atomicity of the memory allocation
 *
 */
static int mp4_cred_alloc_blank(struct cred *cred, gfp_t gfp)
{
	/*
	 * Add your code here
	 * ...
	 */
	mp4_security_t *sec;

	//Sanitizing input
	if(!cred){
		if(printk_ratelimit()){
			pr_alert("input cred structure is nullptr\n");
		}
		return -EINVAL;
	}

	//Allocate memory
	sec = (mp4_security_t*)kmalloc(sizeof(mp4_security_t), gfp);
	if(!sec){
		pr_alert("fail to allocate memory for security entry at cred_alloc");
		return -ENOMEM;
	}
	sec->mp4_flags = MP4_NO_ACCESS;
	cred->security = sec;

	return 0;
}


/**
 * mp4_cred_free - Free a created security label
 *
 * @cred: the credentials struct
 *
 */
static void mp4_cred_free(struct cred *cred)
{
	/*
	 * Add your code here
	 * ...
	 */
	mp4_security_t *sec;

	// check input
	if(cred == NULL){
		pr_alert("input cred structure is nullptr\n");
		return;
	}

	sec = cred->security;
	// check input
	if(sec == NULL){
		pr_alert("cred's sec field is nullptr\n");
		return;
	}
	kfree(sec);
	cred->security = NULL;
}

/**
 * mp4_cred_prepare - Prepare new credentials for modification
 *
 * @new: the new credentials
 * @old: the old credentials
 * @gfp: the atomicity of the memory allocation
 *
 */
static int mp4_cred_prepare(struct cred *new, const struct cred *old,
			    gfp_t gfp)
{
	mp4_security_t *sec;

	// check input
	if(old == NULL){
		pr_alert("input cred structure is nullptr\n");
		mp4_cred_alloc_blank(new, gfp);
		return 0;
	}

	sec = old->security;
	// check input
	if(sec == NULL){
		pr_alert("cred's sec field is nullptr\n");
		mp4_cred_alloc_blank(new, gfp);
		return 0;
	}

	//Allocate
	sec = kmemdup(old->security, sizeof(mp4_security_t), gfp);
	if(!sec){
		pr_alert("fail to allocate memory for security entry at cred_prepare");
		return -ENOMEM;
	}

	new->security = sec;
	return 0;
}

/**
 * mp4_inode_init_security - Set the security attribute of a newly created inode
 *
 * @inode: the newly created inode
 * @dir: the containing directory
 * @qstr: unused
 * @name: where to put the attribute name
 * @value: where to put the attribute value
 * @len: where to put the length of the attribute
 *
 * returns 0 if all goes well, -ENOMEM if no memory, -EOPNOTSUPP to skip
 *
 */
static int mp4_inode_init_security(struct inode *inode, struct inode *dir,
				   const struct qstr *qstr,
				   const char **name, void **value, size_t *len)
{
	/*
	 * Add your code here
	 * ...
	 */
	mp4_security_t* cur_sec;
	// If the current task credential or its security context is NULL
	if (!current_cred()) {
		pr_alert("current_cred is null at mp4_inode_init_security\n");
	    return -1;
	}

	if (!current_cred()->security) {
		pr_alert("current_cred-> security is null at mp4_inode_init_security\n");
	    return -1;
	}
	cur_sec = current_cred()->security;

	if(!cur_sec){
		pr_alert("current sec is null at mp4_inode_init_security\n");
		return -1;
	}

	if(cur_sec->mp4_flags == MP4_TARGET_SID){
		if(name && value && len){
			*name = kstrdup(XATTR_MP4_SUFFIX, GFP_KERNEL);
			*value = kstrdup("read-write", GFP_KERNEL);
			*len = strlen(*value);
			return 0;
		} else {
			pr_alert("target sid does not have full name value and len at mp4_inode_init_security\n");
			return -1;
		}
	}
	return 0;
}

/**
 * mp4_has_permission - Check if subject has permission to an object
 *
 * @ssid: the subject's security id
 * @osid: the object's security id
 * @mask: the operation mask
 *
 * returns 0 is access granter, -EACCES otherwise
 *
 */
/*
static int mp4_has_permission(int ssid, int osid, int mask)
{
	int rc = -1;
	if(osid == MP4_TARGET_SID){
		// object file is labeled
	switch (ssid)
	{
	case MP4_NO_ACCESS:
		{
			if(printk_ratelimit()){
				pr_info("with labeled object: illegal request to MP4_NO_ACCESS but with mask 0x%x\n", mask);
			}
			return -1;
		}
	case MP4_WRITE_OBJ:
		{
			if(mask & (MAY_READ | MAY_EXEC)){
				if(printk_ratelimit()){
					pr_info("with labeled object: illegal request to MP4_WRITE_OBJ but with mask 0x%x\n", mask);
				}
				return -1;
			}
		}
		break;
	case MP4_READ_OBJ:
		{
			if(mask & (MAY_APPEND | MAY_WRITE | MAY_EXEC)){
				if(printk_ratelimit()){
					pr_info("with labeled object: illegal request to MP4_READ_OBJ but with mask 0x%x\n", mask);
				}
				return -1;
			}
		}
		break;
	case MP4_READ_WRITE:
		{
			if(mask & MAY_EXEC){
				if(printk_ratelimit()){
					pr_info("with labeled object: illegal request to MP4_READ_WRITE but with mask 0x%x\n", mask);
				}
				return -1;
			}
		}
		break;
	case MP4_EXEC_OBJ:
		{
			
			if(mask & (MAY_APPEND | MAY_WRITE)){
				if(printk_ratelimit()){
					pr_info("with labeled object: illegal request to MP4_EXEC_OBJ but with mask 0x%x\n", mask);
				}
				return -1;
			}
		}
		break;
	case MP4_READ_DIR:
		{
			if(mask & (MAY_APPEND | MAY_WRITE)){
				if(printk_ratelimit()){
					pr_info("with labeled object: illegal request to MP4_READ_DIR but with mask 0x%x\n", mask);
				}
				return -1;
			}	
		}
	case MP4_RW_DIR:
		{
			//allow
			return 0;
		}
	default:
		break;
	}
	}else{
		// object is not labeled
		switch (ssid)
		{
		case MP4_NO_ACCESS:
			{
				return 0;
			}
			break;
		case MP4_WRITE_OBJ:
			{
				if(mask & (MAY_APPEND | MAY_WRITE | MAY_EXEC)){
					if(printk_ratelimit()){
						pr_info("without labeled object: illegal request to MP4_WRITE_OBJ but with mask 0x%x\n", mask);
					}
					return -1;
				}
			}
			break;
		case MP4_READ_OBJ:
			{
				if(mask & (MAY_APPEND | MAY_WRITE | MAY_EXEC)){
					if(printk_ratelimit()){
						pr_info("without labeled object: illegal request to MP4_READ_OBJ but with mask 0x%x\n", mask);
					}
					return -1;
				}
			}
			break;
		case MP4_READ_WRITE:
			{
				if(mask & (MAY_APPEND | MAY_WRITE | MAY_EXEC)){
					if(printk_ratelimit()){
						pr_info("without labeled object: illegal request to MP4_READ_WRITE but with mask 0x%x\n", mask);
					}
					return -1;
				}
			}
			break;
		case MP4_EXEC_OBJ:
			{
				if(mask & (MAY_APPEND | MAY_WRITE)){
					if(printk_ratelimit()){
						pr_info("without labeled object: illegal request to MP4_EXEC_OBJ but with mask 0x%x\n", mask);
					}
					return -1;
				}
			}
			break;
		default:
			break;
		}
	}
	return rc;
}*/

/**
 * mp4_inode_permission - Check permission for an inode being opened
 *
 * @inode: the inode in question
 * @mask: the access requested
 *
 * This is the important access check hook
 *
 * returns 0 if access is granted, -EACCES otherwise
 *
 */
/*
static int mp4_inode_permission(struct inode *inode, int mask)
{

	const struct mp4_security *cur_sec;
	struct dentry *dentry;
	char* buf;
	char* path;
	int osid, ssid;

	if(!inode){
		pr_alert("inode is nullptr at mp4_inode_permission\n");
		return 0;
	}

	buf = kmalloc(PATH_BUF * sizeof(char), GFP_KERNEL);
	if(!buf){
		pr_alert("cannot allocate buffer at mp4_inode_permission\n");
		return 0;
	}
	buf[PATH_BUF - 1] = '\0';

	dentry = d_find_alias(inode);
	if(!dentry){
		dput(dentry);
		kfree(buf);
		pr_alert("dentry is nullptr at mp4_inode_permission\n");
		return 0;
	}

	path = dentry_path_raw(dentry, buf, PATH_BUF);
	dput(dentry);
	kfree(buf);
	if(mp4_should_skip_path(path)){
		return 0;
	}

	// If the current task credential or its security context is NULL
	if (!current_cred()) {
		if(printk_ratelimit()){
			pr_alert("current_cred is null at mp4_inode_init_security\n");
		}
	    return -EACCES;
	}

	if (!current_cred()->security) {
		if(printk_ratelimit()){
			pr_alert("current_cred-> security is null at mp4_inode_init_security\n");
		}
	    return -EACCES;
	}
	cur_sec = current_cred()->security;
	if(!cur_sec){
		if(printk_ratelimit()){
			pr_alert("current sec is null at mp4_inode_init_security\n");
		}
		return 0;
	}

	osid = get_inode_sid(inode);
	if (osid < 0) {
	    osid = MP4_NO_ACCESS;
	}
	ssid = cur_sec->mp4_flags;
	if(ssid != MP4_TARGET_SID && S_ISDIR(inode->i_mode)){
		//first not labeled file, second is a directory, directly pass
		return 0;
	}
	if (mp4_has_permission(ssid, osid, mask)) {
	    return -EACCES;
	}

	return 0;
}*/

static int mp4_has_permission(int ssid, int osid, int mask)
{
	/*
	 * Add your code here
	 * ...
	 */
    	
    	// If there are no masks about the MAY_READ/MAY_WRITE/MAY_ACCESS/MAY_EXEC
	// pass the permission control to the Linux default access control
	mask &= (MAY_READ|MAY_WRITE|MAY_APPEND|MAY_ACCESS|MAY_EXEC);
	if (!mask)
            goto PERMIT;
    	
    	switch(osid) {
	    // May not be accessed by target, but may be any others
	    // So if the ssid is target, just deny
	    case MP4_NO_ACCESS:
		if (ssid == MP4_TARGET_SID)
		    goto DENY;
		else 
		    goto PERMIT;
	    // May only be read by anyone
	    case MP4_READ_OBJ:
		if ((mask & (MAY_READ)) == mask)
		    goto PERMIT;
		else 
		    goto DENY;
	    // May be read/write/append by target 
	    // and read by others
	    case MP4_READ_WRITE:
		if (ssid == MP4_TARGET_SID)
		    if ((mask & (MAY_READ | MAY_WRITE | MAY_APPEND)) == mask)
			goto PERMIT;
		    else
			goto DENY;
		else 
		    if ((mask & (MAY_READ)) == mask)
			goto PERMIT;
		    else 
			goto DENY;
	    // May be written/appended by target
	    // and only read by others
	    case MP4_WRITE_OBJ:
		if (ssid == MP4_TARGET_SID)
		    if ((mask & (MAY_WRITE | MAY_APPEND)) == mask)
			goto PERMIT;
		    else 
			goto DENY;
		else
		    if ((mask & (MAY_READ)) == mask)
			goto PERMIT;
		    else 
			goto DENY;
	    // May be read/executed by all
	    case MP4_EXEC_OBJ:
		if ((mask & (MAY_READ | MAY_EXEC)) == mask)
		    goto PERMIT;
		else
		    goto DENY;
	    // May be read/exec/access by all
	    case MP4_READ_DIR:
		if (ssid == MP4_TARGET_SID)
		    if ((mask & (MAY_READ | MAY_EXEC | MAY_ACCESS)) == mask)
			goto PERMIT;
		    else 
			goto DENY;
		else 
		    goto PERMIT;
	    // May be modified by all, so just permit
	    case MP4_RW_DIR:
		if (ssid == MP4_TARGET_SID)
                    goto PERMIT;
		else 
		    goto PERMIT;
	}
PERMIT:
	return 0;
DENY:
	return -EACCES;
}

/**
 * mp4_inode_permission - Check permission for an inode being opened
 *
 * @inode: the inode in question
 * @mask: the access requested
 *
 * This is the important access check hook
 *
 * returns 0 if access is granted, -EACCES otherwise
 *
 */
static int mp4_inode_permission(struct inode *inode, int mask)
{
	/*
	 * Add your code here
	 * ...
	 */
    	struct dentry *de;
	const struct cred *cur_cred = current_cred();
	struct mp4_security *mp4_ctx;
	char *res, *buf;
	int osid, ssid;

    	if (!inode)
	    return 0;

	// Find the dentry from inode
	de = d_find_alias(inode);
	if (!de)
	    return 0;

	buf = kzalloc(255 * sizeof(char), GFP_KERNEL);
	if (!buf) {
	    dput(de);
	    return 0;
	}

	// Get the dentry corresponding path
	res = dentry_path_raw(de, buf, 255);
	if (IS_ERR(res)) {
	    pr_err("%s, could not find path\n", __func__);
	    kfree(buf);
	    dput(de);
	    return 0;
	}

	// Skip the pathes which has the prefixes described in helper func
	if (mp4_should_skip_path(res)) {
	    kfree(buf);
	    dput(de);
	    return 0;
	}

	// Get the object sid
	osid = get_inode_sid(inode);
	if (osid < 0) {
	    /*pr_err("%s, could not get osid\n", __func__);*/
	    osid = MP4_NO_ACCESS;
	}

	kfree(buf);
	dput(de);

	// Check the current task credential
	if (!cur_cred) 
	    return 0;

	// Get the sid of the current task
	ssid = MP4_NO_ACCESS;
	mp4_ctx = cur_cred->security;
	if (mp4_ctx) 
	    ssid = mp4_ctx->mp4_flags;

	/*pr_info("%s, ssid %d, osid %d\n", res, ssid, osid);*/

	// Check the permissions
	if (mp4_has_permission(ssid, osid, mask)) {
	    pr_info("%s, DENY: ssid %d, osid %d, mask 0x%x\n", res, ssid, osid, mask);
	    return -EACCES;
	}

	return 0;
}


/*
 * This is the list of hooks that we will using for our security module.
 */
static struct security_hook_list mp4_hooks[] = {
	/*
	 * inode function to assign a label and to check permission
	 */
	LSM_HOOK_INIT(inode_init_security, mp4_inode_init_security),
	LSM_HOOK_INIT(inode_permission, mp4_inode_permission),

	/*
	 * setting the credentials subjective security label when laucnhing a
	 * binary
	 */
	LSM_HOOK_INIT(bprm_set_creds, mp4_bprm_set_creds),

	/* credentials handling and preparation */
	LSM_HOOK_INIT(cred_alloc_blank, mp4_cred_alloc_blank),
	LSM_HOOK_INIT(cred_free, mp4_cred_free),
	LSM_HOOK_INIT(cred_prepare, mp4_cred_prepare)
};

static __init int mp4_init(void)
{
	/*
	 * check if mp4 lsm is enabled with boot parameters
	 */
	if (!security_module_enable("mp4"))
		return 0;

	pr_info("mp4 LSM initializing..");

	/*
	 * Register the mp4 hooks with lsm
	 */
	security_add_hooks(mp4_hooks, ARRAY_SIZE(mp4_hooks));

	return 0;
}

/*
 * early registration with the kernel
 */
security_initcall(mp4_init);
