# CS423-MP1

##  How to run the program

Download
------
    git clone https://github.com/cs423-uiuc-sp20/proj_sp20_f1mm.git

### Build the kernel module
before install the kernel, we need to build the kernel
```
cd theFolderYouChoose
make

```
after this, there should be a file named mp1.ko

### Install the kernel module 
In order to use the moudule, we need to install the kernel into our system, the command show below

```
sudo insmod mp1.ko
```

### Check the install status
You can check the status by this command

```
lsmod
```
If the install succeed, there will be an entry named mp1

### How to use the module
You can test the kernel by running the userapp which is a programm registered the kernel module

```
./userapp
```
During the programming running, you can check the cpu used time by 
```
cat /proc/mp1/status
```
It will show the cpu used time by the userapp

### How to uninstall the module
You can remove the module by
```
sudo rmmod mp1
```
